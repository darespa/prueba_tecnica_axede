var express = require('express');
var router = express.Router();

var controlMain =  require('../bin/controladores/main')

/* GET home page. */
router.get('/', function(req, res, next) {
  res.render('index', { title: 'Express' });
});


router.get('/leer_sedes',  function(req, res, next) {
    controlMain.leer_sedes().then( r  =>{
    res.json({"lista":r});
  })
  
});


module.exports = router;
