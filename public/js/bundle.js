Vue.component('sedes', {
    props: ['sede'],
    template: `   <div class="demo-card-wide mdl-card mdl-shadow--2dp" style="margin:10px;">
                            <div class="mdl-card__title">
                            <h2 class="mdl-card__title-text">{{sede.NOMBRE}}</h2>
                            </div>
                            <div class="mdl-card__supporting-text">
                            HABITACIONES VIP  {{sede.HABITACIONES_VIP}} </br>
                            TARIFA  {{sede.TARIFA_BAJA}}
                            </div>
                            <div class="mdl-card__actions mdl-card--border">
                            <a class="mdl-button mdl-button--colored mdl-js-button mdl-js-ripple-effect">
                            RESERVAR
                            </a>
                            </div>
                            <div class="mdl-card__menu">
                         
                            </div>
                        </div>
`
});



var app = new Vue({
    el: '#CONTENEDOR_MAIN',
    data: {
      sedes:[],
      mostrarmodal:false,
    },
    methods:{
        mostrar_modal:function(){
            console.log("entro")
            this.mostrarmodal= !this.mostrarmodal;
        },
        trae_hoteles: function(){
            var self = this;
            fetch('http://localhost:3000/leer_sedes').then(function(response) {
                if(response.ok) {
                    response.json().then(dato => {
                        dato.lista.forEach(sede => {
                            self.sedes.push(sede)
                        });
                        
                    })
                 
                } else {
                  console.log('Respuesta de red OK pero respuesta HTTP no OK');
                }
              })
              .catch(function(error) {
                console.log('Hubo un problema con la petición Fetch:' + error.message);
              });
        }
    },
    mounted(){
        this.trae_hoteles();
    }
  })